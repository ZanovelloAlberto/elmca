// ============= SEARCH NETWORK ===========

async function searchNetwork(ip_i24) {
  var availableIp = [];
  const l = 250
  var prom = Array(l);
  console.log("searchNetwork");
  for (let i = 0; i < l; i++) {
    try {
      prom[i] = (fetch(`${ip_i24}.${i}/`,//  webapp/config/webapp.json`,
      { signal: AbortSignal.timeout(5000), keepalive : false }));
    } catch (error) {
      
    }

  }
  try {
    var u = await Promise.allSettled(prom);
  } catch (error) {
    
  }

  u.forEach((a, i) => {
      if (a.status == "fulfilled") {
          console.log("fulfilled:" + i);
          availableIp.push(i);
      }
  });

  // if (availableIp[0]) {
  //     document.getElementById("label").innerHTML = availableIp.map(x => x + "").join(":")
  // }
  console.log(availableIp);

  return availableIp.map((x) => ip_i24 + "." + x);
}

// ============= ELM INIT ===========

var app = Elm.Main.init({
  node: document.getElementById("elm-app"),
  flags: 0,
});

app.ports.searchNetwork.subscribe(async function (ip) {
  const x = await searchNetwork(ip)
  console.log(x)
  app.ports.getDevicesIp.send(x)
});

app.ports.output.subscribe(function (msg) {
  var [type, value] = msg;
  switch (type) {
    case "info":
      console.info(value)  
    break;
    
      case "log":
        console.log(value)
      
      break;    
    
      case "error":
        console.error(value)
      break;
  
    default:
      console.log("unrecognized type");
      break;
  }
});

// ============= SERVICE WORKER ===========

if ('serviceWorker' in navigator) {
  navigator.serviceWorker.register('sw.js', { scope: './' }).then(e => {
    console.log("then");
  })
} else {
  console.log("unsupported");
}
