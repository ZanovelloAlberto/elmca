function getWebSocketPath(address) {
    if(address.includes("https")){
        address.replace("https","wss")
    }else{
        address.replace("http","ws")
    }
    return address;
}

function wspath(local) {
    var loc = window.location,
        new_uri;
    if (loc.protocol === "https:") {
        new_uri = "wss:";
    } else {
        new_uri = "ws:";
    }

    if (loc.host.length == 0) {
        new_uri += "127.0.0.1:8080";
    } else {
        new_uri += "//" + loc.host;
        new_uri += loc.pathname + local;
    }
    return new_uri;
}
var socket;

function connect(address) {
    socket = new WebSocket(address);

    socket.onopen = function (e) {
        socket.send(JSON.stringify({ header: "subscribe" }));
        app.ports.connectionError.send(false);
    };

    socket.onmessage = function (e) {
        try {
            var msg = JSON.parse(e.data);
            if (msg.hasOwnProperty("version") && (typeof msg.version === 'string' || msg.version instanceof String)) {
                app.ports.versionPort.send(msg.version);
            }
            if (msg.hasOwnProperty("update")) {
                app.ports.dataUpdate.send(msg.update);
            }
            if (msg.hasOwnProperty("wifi")) {
                app.ports.wifiConfig.send(msg.wifi);
            }
            if (msg.hasOwnProperty("networks")) {
                app.ports.wifiNetworks.send(msg.networks);
            }
            if (msg.hasOwnProperty("error")) {
                app.ports.deviceError.send(msg.error);
            }
        } catch (error) {
            console.log(error);
            console.log(e.data);
        }
    };

    socket.onerror = function (err) {
        console.error("Socket encountered error: ", err.message, "Closing socket");
        socket.close();
    };

    socket.onclose = function (e) {
        console.log("Closed socket " + e);
        app.ports.connectionError.send(true);
        setTimeout(function () {
            console.log("Retry connection");
            connect();
        }, 1000);
    };
}


var app = Elm.Main.init({
    node: document.getElementById("elm-app"),
    flags: 0,
});

app.ports.startAll.subscribe(function (ipTarget){
    const r = "ws://" + ipTarget + "/ws";
    console.log(r);
    connect(r);
})

app.ports.updateRemotePars.subscribe(function (message) {
    if (socket.readyState == WebSocket.OPEN) {
        if (message == null) {
            socket.send('{"header":"update"}');
        } else {
            socket.send('{"header":"update", "registers":' + JSON.stringify(message) + "}");
        }
    }
});

if ("serviceWorker" in navigator) {
    navigator.serviceWorker.register("./sw.js").then(x => {
    });
}

