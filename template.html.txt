<!DOCTYPE html>
<html>
<meta charset="UTF-8" />
<meta name="viewport" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="theme-color" content="#317EFB"/>

<head>
  <link rel="manifest" href="manifest.json">
</head>

<body>
  <style>
    {style_css}
  </style>

  <div id="elm-app"></div>
  <script>
    {elm_min_js}
  </script>
  <script>
    {ports_min_js}
  </script>


</body>

</html>