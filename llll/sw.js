

const cacheName = "data-111";
const appShellFiles = [
    "./index.html",
    "./icons/manifest-icon-512.maskable.png",
    "./icons/manifest-icon-192.maskable.png",
    "./manifest.json",
    "./sw.js",
];

self.addEventListener('install', function (e) {

    e.waitUntil(
        (async () => {
            const cache = await caches.open(cacheName);
            await cache.addAll(appShellFiles);
        })()
    );

    console.log('Install event:', e);
});

self.addEventListener('activate', function (e) {
    console.log('Activate event:', e);
});

self.addEventListener("fetch", (e) => {
    e.respondWith(
      (async () => {
        const r = await caches.match(e.request);
        console.log(`[Service Worker] Fetching resource: ${e.request.url}`);
        if (r) {
          return r;
        }
        const response = await fetch(e.request);
        const cache = await caches.open(cacheName);
        console.log(`[Service Worker] Caching new resource: ${e.request.url}`);
        cache.put(e.request, response.clone());
        return response;
      })()
    );
  });