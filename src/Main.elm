port module Main exposing (main)
import Browser
import Dict exposing (update)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onClick, onInput)
import Http
-- import Svg exposing (..)
-- import Svg.Attributes exposing (..)



-- MAIN


main =
    Browser.element
        { init = init, subscriptions = subscriptions, view = view, update = update }


port searchNetwork : String -> Cmd msg


port getDevicesIp : (List String -> msg) -> Sub msg


-- MODEL


type alias Model =
    { ip : List String
    , s : String
    , selectedIp : Maybe String
    , toggleIpPrefix : Bool
    , isScanning : Bool
    , networkPrefix : String
    }


init : () -> ( Model, Cmd Msg )
init _ =
    ( { ip = []
      , s = ""
      , selectedIp = Nothing
      , toggleIpPrefix = False
      , isScanning = False
      , networkPrefix = "http://192.168.0"
      }
    , Cmd.none
    )


-- UPDATE


type Pages
    = Search
    | Main


type Msg
    = ConfigurationT (Result Http.Error String)
    | ListDevicesIp (List String)
    | SearchClick
    | SelectedIpMsg String
    | TogglePrefix
    | NetworkPrefixInput String


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        ListDevicesIp l ->
            ( { model | ip = l, isScanning = False}, Cmd.none )

        ConfigurationT (Err _) ->
            ( model, Cmd.none )

        ConfigurationT (Ok conf) ->
            ( { model | s = conf }, Cmd.none )

        SelectedIpMsg s ->
            ( { model | selectedIp = Just s }, Cmd.none )

        SearchClick ->
            ( {model | isScanning = True}, searchNetwork model.networkPrefix )

        TogglePrefix ->
            ( { model | toggleIpPrefix = not model.toggleIpPrefix }, Cmd.none )

        NetworkPrefixInput s -> 
            ({model | networkPrefix = s} , Cmd.none )


-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch
        [ getDevicesIp ListDevicesIp
        ]


-- VIEW


view : Model -> Html Msg
view model =
    let
        search_button = Html.button [ onClick SearchClick, model.isScanning |> disabled  ] [ Html.text "search" ]

        ip_list =
            Html.div [] [ Html.ul [] (List.map (\x -> Html.li [ onClick <| SelectedIpMsg x, tabindex 1 ] [ Html.text x ]) model.ip) ]
    in
    if model.toggleIpPrefix then
        div [] [ Html.h4 [] [ Html.text "Change Prefix Page" ]
        , Html.input [ placeholder "Text to reverse", value model.networkPrefix, onInput NetworkPrefixInput ] []
        , Html.button [ onClick TogglePrefix ] [ Html.text "exit" ] 
        ]
    else
        div []
            [ Html.h4 [] [ Html.text "Scan Page" ]
            , Html.text ("current prefix : " ++ model.networkPrefix)
            , Html.br [] []
            , Html.button [ onClick TogglePrefix ] [ Html.text "change prefix" ]
            , Html.br [] []
            , Html.br [] []
            , search_button
            , ip_list
            , Html.text (Maybe.withDefault "select an ip" model.selectedIp)
            ]
